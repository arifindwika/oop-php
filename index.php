<?php

    // require('animal.php');
    require('Ape.php');
    require('Frog.php');

    $sheep = new animal("shaun");

    echo "Animal Name : " . $sheep->name . "<br>";
    echo "Animal Legs : " . $sheep->legs . "<br>";
    echo "Cold Blooded Animal : " . $sheep->cold_blooded . "<br>";

    $sungokong = new Ape("kera sakti");

    echo "<br><br>Animal Name : " . $sungokong->name . "<br>";
    echo "Animal Legs : " . $sungokong->legs . "<br>";
    $sungokong->yell(); // "Auooo"

    
    $kodok = new Frog("buduk");

    echo "<br><br>Animal Name : " . $kodok->name . "<br>";
    echo "Animal Legs : " . $kodok->legs . "<br>";
    $kodok->jump(); // "Auooo"
?>